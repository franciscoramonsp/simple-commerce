import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../../services/auth.service';
import { ErrorHandlerService } from '../../../services/error-handler.service';
import { first } from 'rxjs/operators';


@Component({
  selector: 'commerce-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  email: string;
  senha: string;

  constructor(private router: Router, private authService: AuthService, private error: ErrorHandlerService) { }

  ngOnInit(): void {}


  login(): void {
    this.authService.login(this.email, this.senha)
    .pipe(first()).subscribe(
      data => this.router.navigate(['/admin']),
      (error) => this.error.openDialog(error.error.error)
    );
  }


}
