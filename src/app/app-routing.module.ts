import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './shared/components/login/login.component';
import { AuthGuardService as AuthGuard } from './guards/auth-guard.service';

const routes: Routes = [
  { path: '', redirectTo: 'catalogo', pathMatch: 'full' },
  { path: 'login' , component: LoginComponent },
  { path: 'catalogo' , loadChildren: './modules/catalogo/catalogo.module#CatalogoModule', canActivate:[AuthGuard] },
  { path: 'admin' , loadChildren: './modules/admin/admin.module#AdminModule', canActivate:[AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
