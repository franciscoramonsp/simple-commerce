import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProdutoService {

  constructor( private http: HttpClient ) { }

  URL = 'http://localhost:8000/api/produtos';

  getAll() {
    return this.http.get<{ produtos: any }>(this.URL);
  }

}
