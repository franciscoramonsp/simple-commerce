import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { HomeComponent } from './components/home/home.component';

const catalogoRoutes: Routes = [
  { path: '' , component: HomeComponent }
];

@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(catalogoRoutes)
  ],
  exports : [
    RouterModule,
    HttpClientModule
  ],
})
export class CatalogoModule { }
