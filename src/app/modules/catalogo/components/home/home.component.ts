import { Component, OnInit } from '@angular/core';
import { ProdutoService } from '../../services/produto.service';

@Component({
  selector: 'commerce-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor( private produtoService: ProdutoService ) { }

  produtos = [];

  ngOnInit() {

    this.produtoService.getAll().subscribe(
      (data) =>  this.produtos = data.produtos,
      (error ) => console.log(error),
    );

  }



}
