import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor( private http: HttpClient ) { }

  API = 'http://127.0.0.1:8000/api/';

  login(emailUsuario: string, senhaUsuario: string) {

    return this.http.post < {token: string} > ( this.API + 'auth/login', { email: emailUsuario, senha: senhaUsuario })
      .pipe( map( data => localStorage.setItem('access_token', data.token) ));  }

  logout() {
    localStorage.removeItem('access_token');
  }

  public get loggedIn(): boolean{
    return localStorage.getItem('access_token') !==  null;
  }

}
