import { Injectable } from '@angular/core';
import { JwtHelperService  } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private jwt: JwtHelperService) { }

  usuarioAtual() {
    this.jwt.decodeToken();
  }

}
