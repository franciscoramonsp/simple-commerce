import { Injectable } from '@angular/core';
import { Router  } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService {

  constructor(private router: Router, private jwt: JwtHelperService) { }

  canActivate() {
    if (this.jwt.isTokenExpired() ) {
      this.router.navigate(['/login']);
      return false;
    }
    return true;
  }

}
